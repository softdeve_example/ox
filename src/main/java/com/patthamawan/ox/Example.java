/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.patthamawan.ox;

/**
 *
 * @author Oil
 */
import java.util.Scanner;

class Example {

     public static int add(int a, int b) {
        return a+b;
    }

    public static String chup(char p1, char p2) {
        if (p1 == 's' && p2 == 'p') {
            return "p1";
        }else if (p1 == 'h' && p2 == 's') {
               return "p1";
        }else if (p1 == 'p' && p2 == 'h') {
               return "p1";
        }else if (p1 == 'h' && p2 == 'p') {
            return "p2";
        }else if (p1 == 'p' && p2 == 's') {
               return "p2";
        }else if (p1 == 's' && p2 == 'h') {
               return "p2";
        }
        return "draw";
    }
    
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        System.out.println("Please input player1 (p,h,s,) : ");
        char p1 = kb.next().charAt(0);
        System.out.println("Please input player2 (p,h,s,) : ");
        char p2 = kb.next().charAt(0);
        String winner = chup(p1, p2);
        System.out.println("Winner is "+winner);
    }

    
}

