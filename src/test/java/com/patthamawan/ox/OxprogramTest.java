/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.patthamawan.ox;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author oil
 */
public class OxprogramTest {

    public OxprogramTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    // testCheckVerticalPlayerO "True"
    @Test
    public void testCheckVerticalPlayerOCal1() {
        char currentPlayer = 'O';
        char table[][] = {{'O', '-', '-'},
        {'O', '-', '-'},
        {'O', '-', '-'}};
        int col = 1;
        assertEquals(true, OX.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckVerticalPlayerOCal2() {
        char currentPlayer = 'O';
        char table[][] = {{'-', 'O', '-'},
        {'-', 'O', '-'},
        {'-', 'O', '-'}};
        int col = 2;
        assertEquals(true, OX.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckVerticalPlayerOCal3() {
        char currentPlayer = 'O';
        char table[][] = {{'-', '-', 'O'},
        {'-', '-', 'O'},
        {'-', '-', 'O'}};
        int col = 3;
        assertEquals(true, OX.checkVertical(table, currentPlayer, col));
    }

    // testCheckVerticalPlayerO "False"
    @Test
    public void testCheckVerticalPlayerOCal1False() {
        char currentPlayer = 'O';
        char table[][] = {{'O', '-', '-'},
        {'O', '-', '-'},
        {'-', 'O', '-'}};
        int col = 1;
        assertEquals(false, OX.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckVerticalPlayerOCal2False() {
        char currentPlayer = 'O';
        char table[][] = {{'-', 'O', '-'},
        {'-', 'O', '-'},
        {'-', '-', 'O'}};
        int col = 2;
        assertEquals(false, OX.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckVerticalPlayerOCal3False() {
        char currentPlayer = 'O';
        char table[][] = {{'-', '-', 'O'},
        {'-', '-', 'O'},
        {'-', 'O', '-'}};
        int col = 3;
        assertEquals(false, OX.checkVertical(table, currentPlayer, col));
    }

    //testCheckHorizontalPlayerO
    @Test
    public void testCheckHorizontalPlayerORow1() {
        char currentPlayer = 'O';
        char table[][] = {{'O', 'O', 'O'},
        {'-', '-', '-'},
        {'-', '-', '-'}};
        int row = 1;
        assertEquals(true, OX.checkHorizontal(table, currentPlayer, row));
    }

    @Test
    public void testCheckHorizontalPlayerORow2() {
        char currentPlayer = 'O';
        char table[][] = {{'-', '-', '-'},
        {'O', 'O', 'O'},
        {'-', '-', '-'}};
        int row = 2;
        assertEquals(true, OX.checkHorizontal(table, currentPlayer, row));
    }

    @Test
    public void testCheckHorizontalPlayerORow3() {
        char currentPlayer = 'O';
        char table[][] = {{'-', '-', '-'},
        {'-', '-', '-'},
        {'O', 'O', 'O'}};
        int row = 3;
        assertEquals(true, OX.checkHorizontal(table, currentPlayer, row));
    }

    //testCheckHorizontalPlayerO Return "false"
    @Test
    public void testCheckHorizontalPlayerORow1False() {
        char currentPlayer = 'O';
        char table[][] = {{'O', 'O', '-'},
        {'-', '-', 'O'},
        {'-', '-', '-'}};
        int row = 1;
        assertEquals(false, OX.checkHorizontal(table, currentPlayer, row));
    }

    @Test
    public void testCheckHorizontalPlayerORow2False() {
        char currentPlayer = 'O';
        char table[][] = {{'-', '-', '-'},
        {'O', 'O', '-'},
        {'-', '-', 'O'}};
        int row = 2;
        assertEquals(false, OX.checkHorizontal(table, currentPlayer, row));
    }

    @Test
    public void testCheckHorizontalPlayerORow3False() {
        char currentPlayer = 'O';
        char table[][] = {{'-', '-', '-'},
        {'-', '-', 'O'},
        {'O', 'O', '-'}};
        int row = 3;
        assertEquals(false, OX.checkHorizontal(table, currentPlayer, row));
    }

    //testCheckXLeftPlayerO
    @Test
    public void testCheckXRightPlayerO() {
        char currentPlayer = 'O';
        char table[][] = {{'O', '-', '-'},
        {'-', 'O', '-'},
        {'-', '-', 'O'}};
        assertEquals(true, OX.checkX1(table, currentPlayer));
    }

    @Test
    public void testCheckXLeftPlayerO() {
        char currentPlayer = 'O';
        char table[][] = {{'-', '-', 'O'},
        {'-', 'O', '-'},
        {'O', '-', '-'}};
        assertEquals(true, OX.checkX2(table, currentPlayer));
    }

    //testCheckXLeftPlayerO False
    @Test
    public void testCheckXRightPlayerOFalse() {
        char currentPlayer = 'O';
        char table[][] = {{'-', '-', 'O'},
        {'-', 'O', '-'},
        {'O', '-', '-'}};
        assertEquals(false, OX.checkX1(table, currentPlayer));
    }

    @Test
    public void testCheckXLeftPlayerOFalse() {
        char currentPlayer = 'O';
        char table[][] = {{'O', '-', '-'},
        {'-', 'O', '-'},
        {'-', '-', 'O'}};
        assertEquals(false, OX.checkX2(table, currentPlayer));
    }

    //testCheckDraw
    @Test
    public void testCheckDraw() {
        int count = 9;
        assertEquals(true, OX.checkDraw(count));
    }

    //testCheckXPlayerO
    @Test
    public void testCheckXRPlayerO() {
        char currentPlayer = 'O';
        char table[][] = {{'O', '-', '-'},
        {'-', 'O', '-'},
        {'-', '-', 'O'}};
        assertEquals(true, OX.checkX(table, currentPlayer));
    }

    @Test
    public void testCheckXLPlayerO() {
        char currentPlayer = 'O';
        char table[][] = {{'-', '-', 'O'},
        {'-', 'O', '-'},
        {'O', '-', '-'}};
        assertEquals(true, OX.checkX(table, currentPlayer));
    }

    //CheckWinPlayerO
    @Test
    public void testCheckPlayerOWin() {

        char table[][] = {{'O', 'O', 'O'},
        {'-', '-', '-'},
        {'-', '-', '-'}};

        char currentPlayer = 'O';
        int col = 1;
        int row = 1;
        assertEquals(true, OX.checkWin(table, currentPlayer, col, row));
    }

    // testCheckEvrything PlayerX
    @Test
    public void testCheckVerticalPlayerXCal1() {
        char currentPlayer = 'X';
        char table[][] = {{'X', '-', '-'},
        {'X', '-', '-'},
        {'X', '-', '-'}};
        int col = 1;
        assertEquals(true, OX.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckVerticalPlayerXCal2() {
        char currentPlayer = 'X';
        char table[][] = {{'-', 'X', '-'},
        {'-', 'X', '-'},
        {'-', 'X', '-'}};
        int col = 2;
        assertEquals(true, OX.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckVerticalPlayerXCal3() {
        char currentPlayer = 'X';
        char table[][] = {{'-', '-', 'X'},
        {'-', '-', 'X'},
        {'-', '-', 'X'}};
        int col = 3;
        assertEquals(true, OX.checkVertical(table, currentPlayer, col));
    }

    // testCheckVerticalPlayerX "False"
    @Test
    public void testCheckVerticalPlayerXCal1False() {
        char currentPlayer = 'X';
        char table[][] = {{'X', '-', '-'},
        {'X', '-', '-'},
        {'-', 'X', '-'}};
        int col = 1;
        assertEquals(false, OX.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckVerticalPlayerXCal2False() {
        char currentPlayer = 'X';
        char table[][] = {{'-', 'X', '-'},
        {'-', 'X', '-'},
        {'-', '-', 'X'}};
        int col = 2;
        assertEquals(false, OX.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckVerticalPlayerXCal3False() {
        char currentPlayer = 'X';
        char table[][] = {{'-', '-', 'X'},
        {'-', '-', 'X'},
        {'-', 'X', '-'}};
        int col = 3;
        assertEquals(false, OX.checkVertical(table, currentPlayer, col));
    }

    //testCheckHorizontalPlayerX
    @Test
    public void testCheckHorizontalPlayerXRow1() {
        char currentPlayer = 'X';
        char table[][] = {{'X', 'X', 'X'},
        {'-', '-', '-'},
        {'-', '-', '-'}};
        int row = 1;
        assertEquals(true, OX.checkHorizontal(table, currentPlayer, row));
    }

    @Test
    public void testCheckHorizontalPlayerXRow2() {
        char currentPlayer = 'X';
        char table[][] = {{'-', '-', '-'},
        {'X', 'X', 'X'},
        {'-', '-', '-'}};
        int row = 2;
        assertEquals(true, OX.checkHorizontal(table, currentPlayer, row));
    }

    @Test
    public void testCheckHorizontalPlayerXRow3() {
        char currentPlayer = 'X';
        char table[][] = {{'-', '-', '-'},
        {'-', '-', '-'},
        {'X', 'X', 'X'}};
        int row = 3;
        assertEquals(true, OX.checkHorizontal(table, currentPlayer, row));
    }

    //testCheckHorizontalPlayerX Return "false"
    @Test
    public void testCheckHorizontalPlayerXRow1False() {
        char currentPlayer = 'X';
        char table[][] = {{'X', 'X', '-'},
        {'-', '-', 'X'},
        {'-', '-', '-'}};
        int row = 1;
        assertEquals(false, OX.checkHorizontal(table, currentPlayer, row));
    }

    @Test
    public void testCheckHorizontalPlayerXRow2False() {
        char currentPlayer = 'X';
        char table[][] = {{'-', '-', '-'},
        {'X', 'X', '-'},
        {'-', '-', 'X'}};
        int row = 2;
        assertEquals(false, OX.checkHorizontal(table, currentPlayer, row));
    }

    @Test
    public void testCheckHorizontalPlayerXRow3False() {
        char currentPlayer = 'X';
        char table[][] = {{'-', '-', '-'},
        {'-', '-', 'X'},
        {'X', 'X', '-'}};
        int row = 3;
        assertEquals(false, OX.checkHorizontal(table, currentPlayer, row));
    }

    //testCheckXLeftPlayerX
    @Test
    public void testCheckXRightPlayerX() {
        char currentPlayer = 'X';
        char table[][] = {{'X', '-', '-'},
        {'-', 'X', '-'},
        {'-', '-', 'X'}};
        assertEquals(true, OX.checkX1(table, currentPlayer));
    }

    @Test
    public void testCheckXLeftPlayerX() {
        char currentPlayer = 'X';
        char table[][] = {{'-', '-', 'X'},
        {'-', 'X', '-'},
        {'X', '-', '-'}};
        assertEquals(true, OX.checkX2(table, currentPlayer));
    }

    //testCheckXLeftPlayerX False
    @Test
    public void testCheckXRightPlayerXFalse() {
        char currentPlayer = 'X';
        char table[][] = {{'-', '-', 'X'},
        {'-', 'X', '-'},
        {'X', '-', '-'}};
        assertEquals(false, OX.checkX1(table, currentPlayer));
    }

    @Test
    public void testCheckXLeftPlayerXFalse() {
        char currentPlayer = 'X';
        char table[][] = {{'X', '-', '-'},
        {'-', 'X', '-'},
        {'-', '-', 'X'}};
        assertEquals(false, OX.checkX2(table, currentPlayer));
    }

    //testCheckXPlayerX
    @Test
    public void testCheckXRPlayerX() {
        char currentPlayer = 'X';
        char table[][] = {{'X', '-', '-'},
        {'-', 'X', '-'},
        {'-', '-', 'X'}};
        assertEquals(true, OX.checkX(table, currentPlayer));
    }

    @Test
    public void testCheckXLPlayerX() {
        char currentPlayer = 'X';
        char table[][] = {{'-', '-', 'X'},
        {'-', 'X', '-'},
        {'X', '-', '-'}};
        assertEquals(true, OX.checkX(table, currentPlayer));
    }

    //CheckWinPlayerX
    @Test
    public void testCheckPlayerXWin() {

        char table[][] = {{'X', 'X', 'X'},
        {'-', '-', '-'},
        {'-', '-', '-'}};

        char currentPlayer = 'X';
        int col = 1;
        int row = 1;
        assertEquals(true, OX.checkWin(table, currentPlayer, col, row));
    }

}

